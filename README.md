# Early Modern Dissertations in French Libraries: a Dataset

We create a data set of dissertations in French university libraries via the SUDOC union catalogue and the Bibliothèque Nationale de France via its general catalogue. We retrieve the data via the SRU protocol. 

* The SRU manual for the SUDOC union catalogue: [https://abes.fr/wp-content/uploads/2023/05/guide-utilisation-service-sru-catalogue-sudoc.pdf](https://abes.fr/wp-content/uploads/2023/05/guide-utilisation-service-sru-catalogue-sudoc.pdf)

* The SRU manual for the BNF: [https://www.bnf.fr/sites/default/files/2018-11/service_sru_bnf.pdf](https://www.bnf.fr/sites/default/files/2018-11/service_sru_bnf.pdf)



## Setup

It is recommended to run the code in this repository within a virtual environment. Documentation can be found here: [https://packaging.python.org/en/latest/guides/installing-using-pip-and-virtual-environments/](https://packaging.python.org/en/latest/guides/installing-using-pip-and-virtual-environments/). Using `requirements.txt`, you can then recreate the virtual enviroment used in this project. 

## Repository Structure

* Folder[`results`](`results`)
    
    *  [`bnf_date_result.csv`](results/bnf_date_result.csv): record identifier and year of publication for BNF,
 
    *  [`sudoc_date_result.csv`](results/sudoc_date_result.csv): record identifier and year of publication for SUDOC,
	
	* [`bnf_place_result.csv`](results/bnf_place_result.csv): place of publication and unique identifier for BNF,
	
	* [`sudoc_place_result.csv`](results/sudoc_place_result.csv): place of publication and unique identifier for SUDOC,
	
	* [`bnf_title_result.csv`](results/bnf_title_result.csv): unified title and discipline / subdiscipline for records in the BNF catalogue, dissertation topics
	
	* [`sudoc_title_result.csv`](results/sudoc_title_result.csv): unified title and discipline /subdiscipline for records in the SUDOC catalogue, dissertation topics
    
    * [`bnf_persons_result.csv](results/bnf_persons_result.csv): names and identifiers from authority files for records in the BNF catalogue, 
    
    * [`sudoc_persons_result.csv`](results/sudoc_persons_result.csv): names and identifiers from authority files for records in the SUDOC catalogue,
    
    * [`bnf_lib_result.csv`](results/bnf_lib_result.csv): exemplar data for the BNF catalogue,
    
    * [`sudoc_lib_result.csv`](results/sudoc_lib_result.csv): exemplar data and holding libraries for the SUDOC catalogue,  
	

* Folder [`raw_data_collection`](raw_data_collection)
    
    * Retrieval of catalogue records marked as theses from SUDOC: [`data_collection_sudoc_1.ipynb`](raw_data_collection/data_collection_sudoc_1.ipynb) 
	
	* Parsing XML catalogue records and transformation into a CSV file: [`data_collection_sudoc_2.ipynb`](raw_data_collection/data_collection_sudoc_2.ipynb)
    	
		* Output: [`sudoc_retrieved_raw_data.csv`](raw_data_collection/output/sudoc_retrieved_raw_data.csv)

    * Generation of bigrams of dissertation titles in order to identify additional dissertations: [`data_collection_sudoc-3.ipynb`](raw_data_collection/data_collection_sudoc_3.ipynb)
    
        * Output: [`sudoc_bigram_stats.csv`](raw_data_collection/output/sudoc_bigram_stats.csv) 
 
    * Retrieving records using these bigrams: [`data_collection_sudoc_4.pynb`](raw_data_collection/data_collection_sudoc_4.ipynb)

    * Parsing XML catalogue records for additional dissertations and transformation into a CSV file: [`data_collection_sudoc_5.ipynb`](raw_data_collection/data_collection_sudoc_5.ipynb)
    
        * Output: [`sudoc_raw_data_additional.csv`](raw_data_collection/output/sudoc_raw_data_additional.csv) 
 
    * Retrieval of catalogue records marked as theses from the BNF: ][`data_collection_bnf_1.ipynb`](raw_data_collection/data_collection_bnf_1.ipynb)
 
    * Parsing XML catalogue records and transformation into a CSV file: [`data_collection_bnf_2.ipynb`](raw_data_collection/data_collection_bnf_2.ipynb)
        
        * Output: [`bnf_retrieved_raw_data.csv`](raw_data_collection/output/bnf_retrieved_raw_data.csv)
     
    * Generation of bigrams and retrieval of additional dissertation titles from BNF: [`data_collection_bnf_3.ipynb`](raw_data_collection/data_collection_bnf_3.ipynb)
        
        * Output: [`bnf_bigram_stats.csv`](raw_data_collection/output/bnf_bigram_stats.csv)
    
    * Parsing XML catalogue records for additional dissertations and transformation into a CSV file: [`data_collection_bnf_4.ipynb`](raw_data_collection/data_collection_bnf_4.ipynb)
    
        * Output: [`bnf_raw_data_additional.csv`](raw_data_collection/output/bnf_raw_data_additional.csv)

* Folder [`data_cleaning`](data_cleaning)
    
    *  Removing genres from SUDOC data (oratio, programma, opera), removing titles that are not dissertations from the data that refer to dissertations not explicitly catalogued as such, evaluation of the quality of inferred data: [`data_cleaning_sudoc_1.ipynb`](data_cleaning/data_cleaning_sudoc_1.ipynb)
        
        * Output: [`sudoc_all_diss_clean.csv`](data_cleaning/output/sudoc_all_diss_clean.csv)

    *  Removing genres from BNF data (oratio, programma, opera), removing titles that are not dissertations from the data that refer to dissertations not explicitly catalogued as such, evaluation of the quality of inferred data: [`data_cleaning_bnf_1.ipynb`](data_cleaning/data_cleaning_bnf_1.ipynb)
        
        * Output: [`bnf_all_diss_clean.csv`](data_cleaning/output/bnf_all_diss_clean.csv)

* Folder [`dates`](dates)
    
    * Identifying year of publication in BNF records: [`bnf_dates_1.ipynb`](dates/bnf_dates_1.ipynb)
        
        * Output: [`bnf_date_clean.csv`](dates/output/bnf_date_clean.csv), [`bnf_date_discard.csv`](dates/output/bnf_date_discard.csv)
    
    * Identifying year of publication in SUDOC records: [`sudoc_dates_1.ipynb`](dates/sudoc_dates_1.ipynb)
        
        * Output: [`sudoc_date_clean.csv`](dates/output/sudoc_date_clean.csv), [`sudoc_date_discard.csv`](dates/output/sudoc_date_discard.csv)

* Folder [`places`](places)

	* Aggregating information about the place of publication from various fields in BNF records: [`bnf_places_1.ipynb`](places/bnf_places_1.ipynb)
	
		* Output: [`bnf_place_final.csv`](places/output/bnf_place_final.csv)
		
	* Aggregating information about the place of publication from various fields in SUDOC records: [`sudoc_places_1.ipynb`](places/bnf_places_1.ipynb)
	
		* Output: [`sudoc_place_final.csv`](places/output/bnf_place_final.csv)
		
	* Retrieving CERL identifiers for place names with exactly one entry in the CERL Thesaurus (https://data.cerl.org/thesaurus) : [`reconcile_place_1.ipynb`](places/reconcile_place_1.ipynb)
	
		* Output: [`bnf_place_cerl_1.csv`](places/output/bnf_place_cerl_1.csv), [`sudoc_place_cerl_1.csv`](places/output/bnf_place_cerl_1.csv)
		
	* Manually linking place names and CERL identifiers: [`reconcile_place_2.ipynb`](places/reconcile_place_2.ipynb)
	
		* Output: [`cerl_place_manual_resolved.csv`](places/output/cerl_place_manual_resolved.csv)
		
	* Retrieving additional information about places from CERL: the URL of the  authority record, the ID of the authority record, the preferred human-readable place name in CERL, latitude, longitude, the DBpedia identifier, the GND identifier: [`reconcile_place_3.ipynb`](places/reconcile_place_3.ipynb)
	
		* Output: [`cerl_place_data.csv`](places/output/cerl_place_data.csv)
		
	* Aggregation of final result: [`reconcile_place_4.ipynb`](places/reconcile_place_4.ipynb)
	
* Folder [`titles`](titles)

	* Unifying titles of dissertations in the BNF catalogue, identification of terms that belong to disciplines and subdisciplines, infering the discipline a dissertation title seems to belong to, indication of the topic of the title: [`bnf_titles_1.ipynb`](titles/bnf_titles_1.ipynb)
	
		* Output: [`bnf_title_with_disc.csv`](titles/output/bnf_title_with_disc.csv)
	
	* Unifying titles of dissertations in the SUDOC catalogue, identification of terms that belong to disciplines and subdisciplines, infering the discipline a dissertation title seems to belong to, indication of the topic of the title: [`sudoc_titles_1.ipynb`](titles/sudoc_titles_1.ipynb)
	
		* Output: [`sudoc_title_with_disc.csv`](titles/output/sudoc_title_with_disc.csv)		

* Folder [`persons`](persons)

    * Removing SUDOC records with no references to persons, creation of a list of persons with IDRef unique identifier and of a list of persons without such an identifier: [`sudoc_persons_1.ipynb`](persons/sudoc_persons_1.ipynb)
¸
        * Output: [`sudoc_no_per_recs.csv`](persons/output/sudoc_no_per_recs.csv), [`sudoc_pers_with_id.csv¸](persons/output/sudoc_pers_with_id.csv), [`sudoc_pers_no_id.csv`](persons/output/sudoc_pers_no_id.csv)
    
    * Removing BNF records with no references to persons, creation of a list of persons with BNF unique identifier and of a list of persons without such an identifier: [`bnf_persons_1.ipynb`](persons/persons/bnf_persons_1.ipynb)
 
        * Output: [`bnf_no_per_recs.csv`](persons/output/sudoc_no_per_recs.csv), [`bnf_pers_with_id.csv¸](persons/output/sudoc_pers_with_id.csv), [`bnfpers_no_id.csv`](persons/output/sudoc_pers_no_id.csv)
     
    * Downloading authority files for BNF IDs: [`bnf_rec_pers_1.ipynb`](persons/bnf_rec_pers_1.ipynb)
        
      * Output: files in [output/retrieved_data/bnf/](persons/output/retrieved_data/bnf/).
    
    * Parsing authority files for BNF IDs: [`bnf_rec_pers_2.ipynb`](persons/bnf_rec_pers_2.ipynb)
        
        * Output: [`bnf_persons_result.csv`](results/bnf_persons_result.csv)
    
    * Additional data cleaning of final BNF result: [`bnf_pers_agg_1.ipynb`](persons/bnf_pers_agg_1.ipynb)
 
      * Output: [`bnf_persons_result.csv`](results/bnf_persons_result.csv)    
    
    * Downloading authority files for SUDOC IDs: [`sudoc_rec_pers_1.ipynb`](persons/sudoc_rec_pers_1.ipynb)
        
        * Output: files in [output/retrieved_data/sudoc/](persons/output/retrieved_data/sudoc/).
    
    * Parsing authority files for SUDOC IDs: [`sudoc_rec_pers_2.ipynb`](persons/sudoc_rec_pers_2.ipynb)
        
        * Output: [`sudoc_ids_1.csv`](persons/output/sudoc_ids_1.csv)
    
    * Finding CERL thesaurus IDs for unresolved names in SUDOC: [`sudoc_rec_pers_3.ipynb`](persons/sudoc_rec_pers_3.ipynb)
        
        * Output: [`cerl_sudoc_reconc_1.csv`](persons/output/cerl_sudoc_reconc_1.csv)
        
    * Parsing CERL authority files: [`sudoc_rec_pers_4.ipynb`](persons/sudoc_rec_pers_4.ipynb)
 
        * Output: [`sudoc_cerl_gnd.csv`](persons/output/sudoc_cerl_gnd.csv)
     
    * Getting VIAF information for CERL identified persons via GND: [`sudoc_rec_pers_5.ipynb`](persons/sudoc_rec_pers_5.ipynb)
 
        * Output: [`cerl_gnd_viaf.csv`](persons/output/cerl_gnd_viaf.csv)
        
    * Integrating SUDOC and CERL data: [`sudoc_rec_pers_6.ipynb`](persons/sudoc_rec_pers_6.ipynb)
      
        * Output: [`sudoc_persons_result.csv`](results/sudoc_persons_result.csv)
     
    * Additional data cleaning: [`sudoc_agg_pers_1.ipynb`](persons/sudoc_agg_pers_1.ipynb) 

        * Output: [`sudoc_persons_result.csv`](results/sudoc_persons_result.csv)
     
* Folder [`libraries`](libraries/)
    
    * Capturing data about exemplars in the BNF catalogue: [`bnf_libraries_1.ipynb`](libraries/bnf_libraries_1.ipynb)
        
        * Output: [`bnf_lib_result.csv`](results/bnf_lib_result.csv)
        
    * Creating a list of numerical library identifiers in SUDOC: [`sudoc_libraries_1.ipynb`](libraries/sudoc_libraries_1.ipynb)
    
        * Output: [`sudoc_lib_list.csv`](libraries/output/sudoc_lib_list.csv)
    
    * Resolving library identifiers, aggregating exemplar data and library identifiers for libraries in SUDOC, counting exemplars per library: [`sudoc_libraries_2.ipynb`](libraries/sudoc_libraries_2.ipynb)
        
        * Output: [`sudoc_lib_result.csv`](results/sudoc_lib_result.csv) , [`sudoc_lib_counts.csv`](output/sudoc_lib_counts.csv)
     
* Folder [`aggregation`](aggregation)
    
    * Aggregating BNF results: [`bnf_agg_1.ipynb`](aggregation/bnf_agg_1.ipynb)
 
        * Output: [`bnf_all_raw.csv`](output/bnf_all_raw.csv)
     
    * Deduplication of BNF data: [`bnf_agg_2.ipynb`](aggregation/bnf_agg_2.ipynb)
        
        * Output: [`bnf_deduplicated.csv`](output/bnf_deduplicated.csv)       

    * Unifying names and aggregating SUDOC results: [`sudoc_agg_1.ipynb`](aggregation/sudoc_agg_1.ipynb)
        
        * Output: [`sudoc_all_raw.csv`](output/sudoc_all_raw.csv)
    
    * Deduplication of SUDOC data: [`sudoc_agg_2.ipynb`](aggregation/sudoc_agg_2.ipynb)
        
        * Output: [`sudoc_deduplicated.csv`](output/sudoc_deduplicated.csv)
    
    * Aggregation of SUDOC and BNF data: [`sudoc_bnf_agg_1.ipynb`](aggregation/sudoc_agg_2.ipynb)
        
        * Output: [`bronze_all.csv`](results/bronze_all.csv), [`silver_libr.csv`](results/silver_libr.csv), [`silver_place.csv`](results/silver_place.csv), [`silver_faculty.csv`](results/silver_faculty_csv), [`silver_persons.csv`](results/silver_persons.csv), [`gold_date_pers_place.csv`](results/gold_date_pers_place.csv), [`gold_all.csv`](results/gold_all.csv)